#include "test.h"
#include "mem.h"
#include <stddef.h>


void* heap = NULL;
void heap_create(size_t size) {
	heap = heap_init(size);
	debug_heap(stdout, heap);
	fprintf(stdout, "Heap initialized\n\n");
}


void test1() {
    void* ptr = _malloc(500);

    if (!ptr) {
        fprintf(stderr, "Error in test 1\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
    _free(ptr);
    debug_heap(stdout, heap);

}

void test2() {
    void* ptr1 = _malloc(500);
    void* ptr2 = _malloc(2000);

    if (!ptr1 || !ptr2) {
        fprintf(stderr, "Error in test 2\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
    _free(ptr1);
    debug_heap(stdout, heap);	

}

void test3() {
    void* ptr1 = _malloc(1000);
    void* ptr2 = _malloc(2048);
    void* ptr3 = _malloc(4096);
    if (!ptr1 || !ptr2 || !ptr3) {
        fprintf(stderr, "Error in test 3\n\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(ptr1);
    debug_heap(stdout, heap);
    _free(ptr2);
    debug_heap(stdout, heap);

}

void test4() {
    void* ptr1 = _malloc(1000);
    void* ptr2 = _malloc(2048);
    void* ptr3 = _malloc(12000);

    if (!ptr1 || !ptr2 || !ptr3) {
        fprintf(stderr, "Error in test 4\n\n");
        return;
    }

    debug_heap(stdout, heap);

}

void test5() {
    void* ptr1 = _malloc(8000);
    void* ptr2 = _malloc(25000);
    void* ptr3 = _malloc(100000);

    if (!ptr1 || !ptr2 || !ptr3) {
        fprintf(stderr, "Error in test 5\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
}

