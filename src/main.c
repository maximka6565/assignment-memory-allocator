#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"
#include <test.h>


int main() {
    heap_create(10000);
    printf("Test 1:\n");
    test1();
    printf("Test 2:\n");
    test2();
    printf("Test 3:\n");
    test3();
    printf("Test 4:\n");
    test4();
    printf("Test 5:\n");
    test5();
    return 0;
}

